import torch
import torch.nn as nn
import math

'''We use the Character Embedding Transformer model to generate our customized character-based word embeddings.
   This model is based on a encoder-only architecture
   The word embeddings are trained on the book itself, and then used to annotate the book
   Which means the word embeddings are only used to train a book based model, and not a general model'''

class PositionalEncoding(nn.Module):
    def __init__(self, d_model, max_len=5000):
        super(PositionalEncoding, self).__init__()
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        # Make embeddings relatively larger
        x = x * math.sqrt(self.pe.size(-1))
        # Add constant to embedding
        x = x + self.pe[:x.size(0), :]
        return x


# The CharEmbeddingTransformer model with position encoding
class CharEmbeddingTransformerWithPosEnc(nn.Module):
    def __init__(self, num_chars, d_model, nhead):
        super(CharEmbeddingTransformerWithPosEnc, self).__init__()
        self.embedding = nn.Embedding(num_chars, d_model)
        self.positional_encoding = PositionalEncoding(d_model)
        self.self_attention = SelfAttention(d_model, nhead)
        self.output_layer = nn.Linear(d_model, num_chars)  # Ensure output matches vocab size

    def forward(self, x):
        x = self.embedding(x)
        x = self.positional_encoding(x)
        x = self.self_attention(x)
        x = self.output_layer(x)  # Pass through output layer
        return x


# The self-attention class
class SelfAttention(nn.Module):
    def __init__(self, d_model, nhead):
        super(SelfAttention, self).__init__()
        self.attention = nn.MultiheadAttention(d_model, nhead)

    def forward(self, x):
        return self.attention(x, x, x)[0]
